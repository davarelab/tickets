# Tickets



## How review the project

Open de project and start it with java 1.8. This going to execute in the port 8080.
For the database, the project use H2, so when the service is finished, the tickets data doesn't exist anymore

With swagger, in http://localhost:8080/swagger-ui.html, we can find de documentation for endpoints explained in the next lines: 

## Save ticket
We can create a ticket with a POST request sending the information about the new ticket.
```
http://localhost:8080/tickets
```
#### Body request
```
{
    "userName":"jvelasquez",
    "createdDate":"2019-10-01T11:45:00.000Z",
    "updatedDate":"2019-10-01T11:45:00.000Z",
    "status": "CLOSE"
}
```
<img src="img/2022-10-25 04_15_18-Postman.png">

## Save ticket in bulk process
With this endpoint, we can add a lot of tickets in only one GET request. This take de data for random process.
Only we need put in the url how many tickets we want to insert:

```
http://localhost:8080/tickets/insert-bulk/100
```
<img src="img/2022-10-25 04_17_31-Postman.png">

## Get ticket by id
When we have data, we can find a ticket sending the id through a GET request. Only we need put de id in the url:
```
http://localhost:8080/tickets/1
```
<img src="img/2022-10-25 04_18_46-Postman.png">

## Get all tickets with pagination
It's recommended insert a lot of tickets for use this endpoint. 
In a GET request, put the "_start" and "_end" register that we want to obtain in the url how is showed next:
```
http://localhost:8080/tickets?_start=1&_end=100
```
In the example, we are going to get the information since 1 to 100 register.
This endpoint can't be tested with swagger, but you can use postman or any browser putting the url like the example

<img src="img/2022-10-25 04_11_31-Postman.png">

## Update ticket
For update a ticket, we have a PUT request where in the url put the id of ticket we want to modify: 
```
http://localhost:8080/tickets/1
```
And in the boy request, put the information that we want to change
#### Body request
```
{
	"userName":"sgomez",
	"createdDate":"2021-10-01T11:45:00.000Z",
	"updatedDate":"2021-10-01T11:45:00.000Z",
	"status": "OPEN"
}
```

<img src="img/2022-10-25 04_19_40-Postman.png">
## Delete ticket
For delete a ticket, we have a DELETE request where we only need put in the url the ticket id that we want to delete: 

```
http://localhost:8080/tickets/1
```
<img src="img/2022-10-25 04_20_35-Postman.png">
***

