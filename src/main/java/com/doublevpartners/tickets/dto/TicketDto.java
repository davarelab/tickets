package com.doublevpartners.tickets.dto;


import com.doublevpartners.tickets.util.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class TicketDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String userName;
    private Date createdDate;
    private Date updatedDate;
    private Status status;
}
