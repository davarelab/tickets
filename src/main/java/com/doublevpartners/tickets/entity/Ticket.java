package com.doublevpartners.tickets.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;


import java.util.Date;


@Getter
@Setter
@Entity
@Table(name = "ticket")
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String userName;
    private Date createdDate;
    private Date updatedDate;
    private String status;

}
