package com.doublevpartners.tickets.service;

import com.doublevpartners.tickets.dto.TicketDto;
import com.doublevpartners.tickets.entity.Ticket;
import com.doublevpartners.tickets.mapper.TicketMapper;
import com.doublevpartners.tickets.repository.TicketRepository;
import com.doublevpartners.tickets.util.enums.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Random;


@Service
public class TicketService {

    private final TicketRepository ticketRepository;
    private final TicketMapper ticketMapper;

    @Autowired
    public TicketService(TicketRepository ticketRepository, TicketMapper ticketMapper) {
        this.ticketRepository = ticketRepository;
        this.ticketMapper = ticketMapper;
    }


    public TicketDto findTicketById(Long id) {

        if (id != null) {
            Ticket ticket = ticketRepository.findById(id).orElse(null);
            return ticketMapper.ticketToTicketDto(ticket);
        }
        return null;
    }

    public Page<TicketDto> findAll(Pageable pageable) {

        Page<Ticket> tickets = ticketRepository.findAll(pageable);
        return tickets.map(ticketMapper::ticketToTicketDto);

    }

    public TicketDto saveTicket(TicketDto ticketDto) {
        Ticket ticketSaved = ticketRepository.save(ticketMapper.ticketDtoToTicket(ticketDto));
        return ticketMapper.ticketToTicketDto(ticketSaved);
    }

    public void deleteTicket(Long id) {
        ticketRepository.deleteById(id);
    }

    public void saveBulkData(Integer size) {

        for(int i=1; i<size; i++){
            TicketDto ticketDto = TicketDto.builder()
                    .userName(getRandomUserName())
                    .createdDate(new Date())
                    .updatedDate(new Date())
                    .status( i%2 == 0 ? Status.OPEN : Status.CLOSE)
                    .build();
            ticketRepository.save(ticketMapper.ticketDtoToTicket(ticketDto));
        }
    }

    private String getRandomUserName(){
        int leftLimit = 97;
        int rightLimit = 122;
        int targetStringLength = 10;
        Random random = new Random();

        return random.ints(leftLimit, rightLimit + 1)
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

}
