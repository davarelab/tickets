package com.doublevpartners.tickets.controller;

import com.doublevpartners.tickets.dto.TicketDto;
import com.doublevpartners.tickets.service.TicketService;
import com.doublevpartners.tickets.util.constants.ResponseConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/tickets")
public class TicketController {

    private final TicketService ticketService;

    @Autowired
    public TicketController(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> findTicketById(@PathVariable("id") Long id) {
        TicketDto ticketDto = ticketService.findTicketById(id);

        if (ticketDto == null) {
            return ResponseEntity.badRequest().body(ResponseConstants.NO_FOUND);
        }

        return ResponseEntity.ok().body(ticketDto);

    }

    @GetMapping
    public ResponseEntity<List<TicketDto>> findAllTickets(Pageable pageable) {

        Page<TicketDto> page = ticketService.findAll(pageable);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("X-Total-Count", String.valueOf(page.getTotalElements()));
        return new ResponseEntity<>(page.getContent(),
                httpHeaders,
                HttpStatus.OK);

    }

    @PostMapping
    public ResponseEntity<TicketDto> saveTicket(@RequestBody TicketDto ticketRequest) {

        if (ticketRequest == null) {
            return ResponseEntity.badRequest().body(null);
        }

        TicketDto ticketDtoSaved = ticketService.saveTicket(ticketRequest);
        return ResponseEntity.ok().body(ticketDtoSaved);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateTicket(@RequestBody TicketDto ticketDto, @PathVariable("id") Long id) {
        TicketDto ticketExist = ticketService.findTicketById(id);

        if (ticketExist == null) {
            return ResponseEntity.badRequest().body(ResponseConstants.NO_FOUND);
        }

        ticketDto.setId(id);
        TicketDto ticketDtoSaved = ticketService.saveTicket(ticketDto);
        return ResponseEntity.ok().body(ticketDtoSaved);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteTicket(@PathVariable Long id) {
        TicketDto ticketDto = ticketService.findTicketById(id);

        if (ticketDto == null) {
            return ResponseEntity.badRequest().body(ResponseConstants.NO_FOUND);
        }

        ticketService.deleteTicket(id);
        return ResponseEntity.ok().body(ResponseConstants.DELETE_SUCESSFULL);
    }

    @GetMapping("insert-bulk/{size}")
    public ResponseEntity<Object> insertBulkData(@PathVariable("size") Integer size) {
        ticketService.saveBulkData(size);
        return ResponseEntity.ok().body(ResponseConstants.BULK_INSERT_OK);
    }
}
