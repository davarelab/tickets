package com.doublevpartners.tickets.util.constants;


public class ResponseConstants {
	public static final String BULK_INSERT_OK = "Inserción de datos ejecutado correctamente";
	public static final String NO_FOUND = "No se encontró el ticket";
	public static final String DELETE_SUCESSFULL = "Se eliminó el registro correctamente";
}