package com.doublevpartners.tickets.util.enums;

import lombok.Getter;

@Getter
public enum Status {
    OPEN,
    CLOSE
}
