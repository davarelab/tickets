package com.doublevpartners.tickets.mapper;

import com.doublevpartners.tickets.dto.TicketDto;
import com.doublevpartners.tickets.entity.Ticket;
import org.mapstruct.Mapper;

@Mapper(
        componentModel = "spring"
)
public interface TicketMapper {
    TicketDto ticketToTicketDto(Ticket ticket);
    Ticket ticketDtoToTicket(TicketDto ticket);
}
